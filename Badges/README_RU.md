# Изображения значков

[![Build Status](https://gitlab.com/nindet.ztx/it-can-useful/raw/master/Badges/Go/badge.svg)](https://gitlab.com/nindet.ztx/it-can-useful)

Для создания изображения своего значка используется генератор на сайте https://shields.io/endpoint.

# Создаем схему изображения

Создайте файл json укажите свою схему изображения значка

   - schemaVersion -> (обязательный) Всегда номер 1.
   - label -> (обязательный) Текст слева на значке.
   - message -> (обязательный) Текст справа на значке.
   - color -> По умолчанию: "lightgrey". Поддерживает цвета hex, rgb, rgba, hsl, hsla и css.
   - labelColor -> По умолчанию: "grey". Левый цвет. Это может быть переопределено строкой запроса.
   - logoSvg -> По умолчанию: нет. Строка SVG, содержащая собственный логотип.
   - Вы можете найти все доступные свойства схемы на https://shields.io/endpoint
   
### Пример
```sh
    {
      "schemaVersion":1,
      "label":"GoLang",
      "message":"Go By Task",
      "color":"blue",
      "labelColor": "orange",
      "logoSvg":"<svg xmlns=\"http://www.w3.org/2000/svg\"><path .../></svg>",
      "cacheSeconds": 1800
    }
```

# Создаем ссылку на JSON
Первый способ - создать mock ссылки, например используя https://designer.mocky.io/
Перейдите по ссылке, создайте новый mock, скопируйте в него данные json и нажмите кнопку «Создать». 
Поздравляю, вы получили ссылку.

Второй способ получить ссылку через GitLab
Заполненый json загрузите в GitLab и скопируйте ссылку на получение данных из файла.
### Порядок действий
   - Загрузите в свой публичный репозиторий json файл
   - Зайдите в свой репозиторий GitLab и откройте json
   - пример (https://gitlab.com/nindet.ztx/it-can-useful/blob/master/Badges/Go/go.json)
   - Нажмите на кнопку «Open raw»
   - Скопируйте открывающуюся ссылку на ваши данные JSON
   - пример (https://gitlab.com/nindet.ztx/it-can-useful/raw/master/Badges/Go/go.json)
#
Если мы перейдем по ссылке, мы увидим наши данные, хранящиеся в файле JSON
#### Пример результата перехода
```sh
    {
        "schemaVersion":1,
        "label":"GoLang",
        "message":"Go By Task",
        "color":"blue",
        "labelColor": "orange",
        "logoSvg": "<svg ...
```
# Создаем изображение значка
  - Перейдите на https://shields.io/endpoint и найдите раздел «Customize and test».
  - Затем в поле «url» вставьте свою ссылку (mock или gitlab) на json файл и нажмите «Copy URL Badges»
  - Вставьте полученную ссылкеу в браузер
  - Ваш значок был создан!
### Пример полученной ссылки
```sh
  https://img.shields.io/endpoint?url=https%3A%2F%2Fgitlab.com%2Fnindet.ztx%2F
  it-can-useful%2F-%2Fraw%2Fmaster%2FBadges%2FGo%2Fgo.json
```

# Подключение значка к GitLab
## Первый способ подключения значка, используя ссылку на изображение значка.

Для этого зайдите в свой проект Gitlab, перейдите в «Setting» -> «General», найдите раздел «Badges» и откройте его.

### Заполнение данных
  - В поле «Name» запишите название своего значка
  - В поле «Link» запишите ссылку на действие при нажатии на значок. Получить информацию о действиях со ссылками (https://gitlab.com/help/user/project/badges)
  - В поле «Badge image URL» запишите ссылку на изображение вашего значка.
  - Загрузите значок в проект, нажав кнопку Add badge

## Второй способ подключения значка, предварительно загрузив изображение значка на GitLab
Перейдите по полученной ссылке и откройте инспектор F12 или macOS Option + Option + I
Скопируйте весь код с вкладки Элементы
```sh
   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="141" height="20">
   <script xmlns="" type="text/javascript" charset="utf-8" id="zm-extension"/>
   <linearGradient id="s" x2="0" y2="100%"><
   stop offset="0" stop-color="#bbb" stop-opacity=".1"/>
   <stop offset="1" stop-opacity=".1"/>
   </linearGradient><clipPath id="r">
   <rect width="141" height="20" rx="3"... </svg>
```
Вставьте код в файл svg, удалите скрипты из кода
`` `<script xmlns =" "type =" text / javascript "charset =" utf-8 "id =" zm-extension "/>` ``
Загрузите файл svg в свой проект Gitlab и откройте его на gitlab (https://gitlab.com/nindet.ztx/it-can-useful/blob/master/Badges/Go/go.svg).
Щелкните правой кнопкой мыши на кнопку Open raw и скопируйте адрес ссылки (https://gitlab.com/nindet.ztx/it-can-useful/-/raw/master/Badges/Go/go.svg).
Мы получили ссылку из GitLab на наше изображение значка
Далее перейдите в «Setting» -> «General», найдите раздел «Badges» и откройте его.

### Детали бейджа
  - В поле «Name» запишите название своего значка
  - В поле «Link» запишите ссылку на действие при нажатии на значок. Получить информацию о действиях со ссылками (https://gitlab.com/help/user/project/badges)
  - В поле «Badge image URL» запишите ссылку на изображение вашего значка.
  - Загрузите значок в проект, нажав кнопку Add badge

Готово, значок создан. Чтобы узнать, зачем нужен значок и как его использовать, смотрите на https://gitlab.com/help/user/project/badges.
