# Badge Images

[![Build Status](https://gitlab.com/nindet.ztx/it-can-useful/raw/master/Badges/Go/badge.svg)](https://gitlab.com/nindet.ztx/it-can-useful)

To create an image of your icon, use the generator at https://shields.io/endpoint.

# Create Image Schema


Create a json file, specify the image scheme of your icon

  - schemaVersion -> Required. Always the number 1.
  - label -> Required The left text, or the empty string to omit the left side of the badge. This can be overridden by the query string
  - message -> Required. Can't be empty. The right text
  - color  -> Default: lightgrey. The right color. Supports the eight named colors above, as well as hex, rgb, rgba, hsl, hsla and css named colors. This can be overridden by the query string
  - labelColor  -> Default: grey. The left color. This can be overridden by the query string
  - logoSvg ->  Default: none. An svg string containing a custom logo.
  - You can find all available properties at https://shields.io/endpoint

# EXAMPLE
```sh
    {
      "schemaVersion":1,
      "label":"GoLang",
      "message":"Go By Task",
      "color":"blue",
      "labelColor": "grey",
      "logoSvg":"<svg xmlns=\"http://www.w3.org/2000/svg\"><path .../></svg>",
      "cacheSeconds": 1800
    }
```

# Create a link to JSON
The first way is to create mock links, for example, using https://designer.mocky.io/
Following the link, create a new mock, copy the json data into it and click the "Create" button
Congratulations, you received the link.

The second way to get the link through GitLab
Fill out your JSON file, upload it to GitLab and copy the link to get data from it.
### Procedural steps
  - Upload a json file to your public repository
  -  Go to your GitLab repository and open the json file
  -  example (https://gitlab.com/nindet.ztx/it-can-useful/-/blob/master/Badges/Go/go.json)
  -  Click on the “Open raw” button
  -  Copy the opened link
  -  example (https://gitlab.com/nindet.ztx/it-can-useful/-/raw/master/Badges/Go/go.json)
#
If we follow the link, we will see our data stored in the JSON file
####  Example Result
```sh
    {
        "schemaVersion":1,
        "label":"GoLang",
        "message":"Go By Task",
        "color":"blue",
        "labelColor": "orange",
        "logoSvg": "<svg ...
```
# Create badge image
 - Go to https://shields.io/endpoint and find the «Customize and test» section
 - Then in the «url» field, paste our link to the json file (mock or gitlab) and select «Copy Badges URL»
 - Paste the received data into the browser
 - Your badge has been created!
### Example of received link
```sh
  https://img.shields.io/endpoint?url=https%3A%2F%2Fgitlab.com%2Fnindet.ztx%2F
  it-can-useful%2F-%2Fraw%2Fmaster%2FBadges%2FGo%2Fgo.json
```
# Connecting a badge to GitLab
## The first way to connect the badge 
Using the link that we received in the previous step. To do this, go to Settings -> General, look for the Badges section and open it.

### Badge Details
 - In the "Name" field, write the name of your badge
 - In the “Link” field, write a link to the action when clicking on the badge. Get information about actions with links (https://gitlab.com/help/user/project/badges)
 - In the «Badge image URL» field, write the previously received link to the image of your badge
 - Submit the badge by clicking the Add badge button

## The second way to connect the badge
Follow the received link and open inspector F12 or macOS ⌘ + Option +  I
Copy the entire code from the Elements tab
```sh
   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="141" height="20">
   <script xmlns="" type="text/javascript" charset="utf-8" id="zm-extension"/>
   <linearGradient id="s" x2="0" y2="100%"><
   stop offset="0" stop-color="#bbb" stop-opacity=".1"/>
   <stop offset="1" stop-opacity=".1"/>
   </linearGradient><clipPath id="r">
   <rect width="141" height="20" rx="3"... </svg>
```
Paste the code into the SVG file, remove the scripts from the code and upload the SVG file to Gitlab
```<script xmlns="" type="text/javascript" charset="utf-8" id="zm-extension"/>```
Open the downloaded svg file on gitlab (https://gitlab.com/nindet.ztx/it-can-useful/blob/master/Badges/Go/go.svg)
Right-click the Open Raw button and copy the link address (https://gitlab.com/nindet.ztx/it-can-useful/-/raw/master/Badges/Go/go.svg)
You got a link from GitLab to your badge image
Next, go to "Settings" -> "General", find the "Badge" section and open it

### Badge Details
 - Under "Name" field, write the name of your badge
 - Under "Link", enter the URL that the badges should point to
 - Under «Badge image URL» field, write the previously received link to the image of your badge
 - Submit the badge by clicking the Add badge button

Done, badge created. To find out why a badge is needed and how a badge is used, go to https://gitlab.com/help/user/project/badges